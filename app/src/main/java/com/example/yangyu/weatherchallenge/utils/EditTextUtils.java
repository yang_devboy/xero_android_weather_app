package com.example.yangyu.weatherchallenge.utils;

import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.widget.EditText;

/**
 * This class provides a set of general purpose utility methods related to edit text.
 */

public final class EditTextUtils {

    /**
     * Validates whether or not an edit text contains a non-empty and not null string.
     *
     * @param editText        The edit text.
     * @param textInputLayout The text input layout wrapper.
     * @param error           The error message to show.
     * @return True if it contains a non-empty and not null string, false otherwise.
     */
    public static boolean validateEditText(final EditText editText, final TextInputLayout textInputLayout,
                                           final String error) {
        final boolean result = !TextUtils.isEmpty(getEditText(editText));
        textInputLayout.setError(!result ? error : null);

        return result;
    }

    /**
     * Obtains the text from a particular edit text.
     *
     * @param editText The edit text.
     * @return The text string.
     */
    public static String getEditText(final EditText editText) {
        return editText.getText().toString().trim();
    }

}
