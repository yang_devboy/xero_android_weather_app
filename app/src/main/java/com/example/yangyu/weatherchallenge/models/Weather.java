package com.example.yangyu.weatherchallenge.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * The weather model.
 */

public final class Weather {
    @SerializedName("main")
    private Main mMain;
    @SerializedName("weather")
    private List<CurrentWeather> mCurrentWeatherList;
    @SerializedName("wind")
    private Wind mWind;
    @SerializedName("dt_txt")
    private String mDate;

    public Main getMain() {
        return mMain;
    }

    public List<CurrentWeather> getCurrentWeatherList() {
        return mCurrentWeatherList;
    }

    public Wind getWind() {
        return mWind;
    }

    public String getDate() {
        return mDate;
    }

    public static class Main {
        @SerializedName("temp")
        private double mTemp;
        @SerializedName("temp_min")
        private double mMinTemp;
        @SerializedName("temp_max")
        private double mMaxTemp;
        @SerializedName("humidity")
        private double mHumidity;

        public double getTemp() {
            return mTemp;
        }

        public double getMinTemp() {
            return mMinTemp;
        }

        public double getMaxTemp() {
            return mMaxTemp;
        }

        public double getHumidity() {
            return mHumidity;
        }
    }

    public static class Wind {
        @SerializedName("speed")
        private double mSpeed;
        @SerializedName("deg")
        private double mDeg;

        public double getSpeed() {
            return mSpeed;
        }

        public double getDeg() {
            return mDeg;
        }
    }

    public static class CurrentWeather {
        @SerializedName("id")
        private long mId;
        @SerializedName("main")
        private String mMain;
        @SerializedName("description")
        private String mDescription;
        @SerializedName("icon")
        private String mIcon;

        public long getId() {
            return mId;
        }

        public String getMain() {
            return mMain;
        }

        public String getDescription() {
            return mDescription;
        }

        public String getIcon() {
            return mIcon;
        }
    }
}
