package com.example.yangyu.weatherchallenge.di.modules;

import android.app.Application;

import com.example.yangyu.weatherchallenge.App;
import com.example.yangyu.weatherchallenge.di.qualifiers.ForApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * The module that corresponds to the application component.
 */

@Module
public final class AppModule {
    private final App mApp;

    public AppModule(final App app) {
        mApp = app;
    }

    @Provides
    @Singleton
    @ForApplication
    Application provideApplication() {
        return mApp;
    }
}
