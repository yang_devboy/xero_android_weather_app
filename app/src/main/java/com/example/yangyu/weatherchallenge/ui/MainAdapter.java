package com.example.yangyu.weatherchallenge.ui;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.yangyu.weatherchallenge.R;
import com.example.yangyu.weatherchallenge.models.Weather;
import com.example.yangyu.weatherchallenge.utils.DateTimeUtils;
import com.example.yangyu.weatherchallenge.utils.WeatherUtils;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * The adapter for the list in main activity.
 */

public final class MainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Weather> mWeatherList = Collections.emptyList();

    @Nullable
    private OnClickListener mOnClickListener;

    /**
     * Constants for different types of views.
     */
    private static final int ITEM_VIEW_TYPE_HEADER = 0;
    private static final int ITEM_VIEW_TYPE_ITEM = 1;

    @Inject
    public MainAdapter() {

    }

    void setOnClickListener(@Nullable OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    void replaceWith(List<Weather> weatherList) {
        mWeatherList = weatherList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return 0 == position ? ITEM_VIEW_TYPE_HEADER : ITEM_VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return mWeatherList.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (ITEM_VIEW_TYPE_HEADER == viewType) {
            return new HeaderViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_weather_details, parent, false));
        }

        return new ItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_list_item_weather_forecast, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        // NOTE the given daily forecast api is not working any more. Have to use the 5 day / 3 hour
        // api which returns multiple weather records for each day.
        final Weather weather = mWeatherList.get(position);

        if (null != weather) {
            if (0 == position) {
                final HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
                final Weather.Main main = weather.getMain();
                final Context context = headerViewHolder.mDateTextView.getContext();
                headerViewHolder.mDateTextView.setText(DateTimeUtils.formatDate(weather.getDate(), Locale.getDefault()));
                headerViewHolder.mTemperatureTextView.setText(
                        context.getString(R.string.text_max_min_temperature, main.getMaxTemp(), main.getMinTemp())
                );
                headerViewHolder.mConditionTextView.setText(
                        context.getString(R.string.text_weather_condition, weather.getCurrentWeatherList().get(0).getMain())
                );
                headerViewHolder.mWindTextView.setText(
                        context.getString(R.string.text_wind_direction,
                                WeatherUtils.getWindDirection(weather.getWind().getDeg()))
                );
                headerViewHolder.mHumidityTextView.setText(
                        context.getString(R.string.text_humidity, main.getHumidity())
                );
            } else {
                final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
                itemViewHolder.mDateTextView.setText(DateTimeUtils.formatDate(weather.getDate(), Locale.getDefault()));

                itemViewHolder.mDateTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mOnClickListener != null) {
                            mOnClickListener.onItemClicked(weather);
                        }
                    }
                });
            }
        }
    }

    /**
     * Provide a reference to the view for the list's header.
     */
    static final class HeaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.date_tv)
        TextView mDateTextView;
        @BindView(R.id.temperature_tv)
        TextView mTemperatureTextView;
        @BindView(R.id.condition_tv)
        TextView mConditionTextView;
        @BindView(R.id.wind_tv)
        TextView mWindTextView;
        @BindView(R.id.humidity_tv)
        TextView mHumidityTextView;

        HeaderViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    /**
     * Provide a reference to the view for the list's normal item.
     */
    static final class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.date_tv)
        TextView mDateTextView;

        ItemViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    interface OnClickListener {
        void onItemClicked(Weather weather);
    }
}
