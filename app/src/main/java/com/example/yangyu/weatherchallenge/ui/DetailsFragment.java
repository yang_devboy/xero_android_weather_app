package com.example.yangyu.weatherchallenge.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.example.yangyu.weatherchallenge.R;
import com.example.yangyu.weatherchallenge.common.BaseFragment;
import com.example.yangyu.weatherchallenge.common.BasePresenter;
import com.example.yangyu.weatherchallenge.utils.DateTimeUtils;
import com.example.yangyu.weatherchallenge.utils.WeatherUtils;

import java.util.Locale;

import butterknife.BindView;

/**
 * The details fragment.
 */

public final class DetailsFragment extends BaseFragment {
    @BindView(R.id.date_tv)
    TextView mDateTextView;
    @BindView(R.id.temperature_tv)
    TextView mTemperatureTextView;
    @BindView(R.id.condition_tv)
    TextView mConditionTextView;
    @BindView(R.id.wind_tv)
    TextView mWindTextView;
    @BindView(R.id.humidity_tv)
    TextView mHumidityTextView;

    private static final String ARG_DATE = "ARG_DATE";
    private static final String ARG_MAX_TEMP = "ARG_MAX_TEMP";
    private static final String ARG_MIN_TEMP = "ARG_MIN_TEMP";
    private static final String ARG_CONDITION = "ARG_CONDITION";
    private static final String ARG_WIND_DEGREE = "ARG_WIND_DEGREE";
    private static final String ARG_HUMIDITY = "ARG_HUMIDITY";

    private String mDate;
    private double mMaxTemp;
    private double mMinTemp;
    private String mCondition;
    private double mWindDegree;
    private double mHumidity;

    public static DetailsFragment newInstance(String date, double maxTemp, double minTemp,
                                              String condition, double windDegree, double humidity) {
        final DetailsFragment fragment = new DetailsFragment();
        final Bundle args = new Bundle();
        args.putString(ARG_DATE, date);
        args.putDouble(ARG_MAX_TEMP, maxTemp);
        args.putDouble(ARG_MIN_TEMP, minTemp);
        args.putString(ARG_CONDITION, condition);
        args.putDouble(ARG_WIND_DEGREE, windDegree);
        args.putDouble(ARG_HUMIDITY, humidity);
        fragment.setArguments(args);

        return fragment;
    }

    @NonNull
    @Override
    public BasePresenter createPresenter() {
        return new BasePresenter();
    }

    @Override
    protected void initArgs(Bundle args) {
        mDate = args.getString(ARG_DATE);
        mMaxTemp = args.getDouble(ARG_MAX_TEMP);
        mMinTemp = args.getDouble(ARG_MIN_TEMP);
        mCondition = args.getString(ARG_CONDITION);
        mWindDegree = args.getDouble(ARG_WIND_DEGREE);
        mHumidity = args.getDouble(ARG_HUMIDITY);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_details;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDateTextView.setText(DateTimeUtils.formatDate(mDate, Locale.getDefault()));
        mTemperatureTextView.setText(getString(R.string.text_max_min_temperature, mMaxTemp, mMinTemp));
        mConditionTextView.setText(getString(R.string.text_weather_condition, mCondition));
        mWindTextView.setText(getString(R.string.text_wind_direction, WeatherUtils.getWindDirection(mWindDegree)));
        mHumidityTextView.setText(getString(R.string.text_humidity, mHumidity));
    }
}
