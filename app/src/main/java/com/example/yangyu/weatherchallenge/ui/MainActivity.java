package com.example.yangyu.weatherchallenge.ui;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.yangyu.weatherchallenge.R;
import com.example.yangyu.weatherchallenge.common.BaseActivity;
import com.example.yangyu.weatherchallenge.common.BasePresenter;
import com.example.yangyu.weatherchallenge.common.Constants;
import com.example.yangyu.weatherchallenge.di.components.DaggerMainActivityComponent;
import com.example.yangyu.weatherchallenge.di.components.MainActivityComponent;
import com.example.yangyu.weatherchallenge.di.modules.MainActivityModule;
import com.example.yangyu.weatherchallenge.models.Weather;
import com.example.yangyu.weatherchallenge.models.WeatherForecast;
import com.example.yangyu.weatherchallenge.utils.EditTextUtils;
import com.example.yangyu.weatherchallenge.utils.Utils;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * The main activity.
 */

public final class MainActivity extends BaseActivity implements MainView, MainAdapter.OnClickListener {
    @BindView(R.id.city_name_layout)
    TextInputLayout mCityNameLayout;
    @BindView(R.id.city_name_et)
    EditText mCityNameEditText;
    @BindView(R.id.forecast_rv)
    RecyclerView mForecastRecyclerView;
    @BindView(R.id.content_placeholder)
    FrameLayout mContentPlaceholder;

    @Inject
    MainPresenter mMainPresenter;

    @Inject
    MainAdapter mMainAdapter;

    private ProgressDialog mProgressDialog;

    @NonNull
    @Override
    public BasePresenter createPresenter() {
        return mMainPresenter;
    }

    @Override
    protected void injectDependencies() {
        final MainActivityComponent mainActivityComponent = DaggerMainActivityComponent.builder()
                .appComponent(getAppComponent())
                .mainActivityModule(new MainActivityModule(this))
                .build();
        mainActivityComponent.inject(this);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    public void showLoading() {
        if (null == mProgressDialog) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.message_dialog_get_weather_forecast));
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
        }
        mProgressDialog.show();
    }

    @Override
    public void hideLoading() {
        if (null != mProgressDialog && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void showContent(WeatherForecast weatherForecast) {
        if (null == mForecastRecyclerView.getAdapter()) {
            // the list has not been initialized
            mForecastRecyclerView.setHasFixedSize(true);
            mForecastRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mForecastRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mForecastRecyclerView.setAdapter(mMainAdapter);
            mMainAdapter.setOnClickListener(this);
        }

        final List<Weather> weatherList = weatherForecast.getWeatherList();
        if (null != weatherList) {
            mMainAdapter.replaceWith(weatherList);
        }
    }

    @Override
    public void showError(Throwable throwable) {
        Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.go_btn)
    public void onGoButtonClicked() {
        if (EditTextUtils.validateEditText(
                mCityNameEditText, mCityNameLayout, getString(R.string.error_invalid_city_name))) {
            Utils.hideSoftKeyboard(this);
            mMainPresenter.getWeatherForecast(Constants.API_KEY,
                    EditTextUtils.getEditText(mCityNameEditText),
                    Constants.API_TEMPERATURE_CELSIUS_UNITS);
        }
    }

    @Override
    public void onItemClicked(Weather weather) {
        if (null != weather) {
            mContentPlaceholder.setVisibility(View.VISIBLE);

            final Weather.Main main = weather.getMain();
            addFragmentToActivity(
                    DetailsFragment.newInstance(weather.getDate(), main.getMaxTemp(),
                            main.getMinTemp(), weather.getCurrentWeatherList().get(0).getMain(),
                            weather.getWind().getDeg(), main.getHumidity()),
                    R.id.content_placeholder,
                    true,
                    android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right
            );
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            mContentPlaceholder.setVisibility(View.GONE);
        }
    }
}
