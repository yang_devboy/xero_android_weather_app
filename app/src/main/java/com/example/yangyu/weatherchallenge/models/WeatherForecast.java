package com.example.yangyu.weatherchallenge.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * The weather forecast model.
 */

public final class WeatherForecast extends ApiResponse {
    @SerializedName("cnt")
    private int mCnt;
    @SerializedName("list")
    private List<Weather> mWeatherList;

    public int getCnt() {
        return mCnt;
    }

    public List<Weather> getWeatherList() {
        return mWeatherList;
    }
}
