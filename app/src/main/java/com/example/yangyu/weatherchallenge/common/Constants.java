package com.example.yangyu.weatherchallenge.common;

/**
 * This class defines all the constants used in the app.
 */

public final class Constants {
    public static final String API_BASE_URL = "http://api.openweathermap.org/data/2.5/";
    public static final String API_KEY = "c4f1ae3d520e4b1298a5366e0bbe9514";
    public static final String API_TEMPERATURE_CELSIUS_UNITS = "metric";
}
