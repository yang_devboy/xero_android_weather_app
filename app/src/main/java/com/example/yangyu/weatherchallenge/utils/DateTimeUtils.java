package com.example.yangyu.weatherchallenge.utils;

import com.orhanobut.logger.Logger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * This class provides a set of general purpose utility methods related to date and time.
 */

public final class DateTimeUtils {

    /**
     * Format a date based on the given locale.
     *
     * @param date   The date to be formatted.
     * @param locale The locale.
     * @return The formatted date string.
     */
    public static String formatDate(final String date, final Locale locale) {
        if (null == date) {
            return "";
        }

        final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", locale);
        dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            final Date formattedDate = dateFormatter.parse(date);
            final DateFormat simpleDateFormatter = SimpleDateFormat.getDateTimeInstance();
            simpleDateFormatter.setTimeZone(TimeZone.getDefault());
            return simpleDateFormatter.format(formattedDate);
        } catch (ParseException e) {
            Logger.e("Failed to parse date due to: " + e.getMessage());
            return "";
        }
    }

}
