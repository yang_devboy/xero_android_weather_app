package com.example.yangyu.weatherchallenge.di.components;

import com.example.yangyu.weatherchallenge.di.modules.MainActivityModule;
import com.example.yangyu.weatherchallenge.di.scopes.PerActivity;
import com.example.yangyu.weatherchallenge.ui.MainActivity;
import com.example.yangyu.weatherchallenge.ui.MainAdapter;
import com.example.yangyu.weatherchallenge.ui.MainPresenter;

import dagger.Component;

/**
 * The component corresponding to the main activity.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules = MainActivityModule.class)
public interface MainActivityComponent {
    void inject(MainActivity activity);

    MainActivity mainActivity();

    MainPresenter mainPresenter();

    MainAdapter mainAdapter();
}
