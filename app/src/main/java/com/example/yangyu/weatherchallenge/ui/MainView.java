package com.example.yangyu.weatherchallenge.ui;

import com.example.yangyu.weatherchallenge.common.BaseView;
import com.example.yangyu.weatherchallenge.models.WeatherForecast;

/**
 * The view interface for the main activity.
 */

interface MainView extends BaseView {

    void showLoading();

    void hideLoading();

    void showContent(WeatherForecast weatherForecast);

    void showError(Throwable throwable);

}
