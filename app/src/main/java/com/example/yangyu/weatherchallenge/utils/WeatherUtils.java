package com.example.yangyu.weatherchallenge.utils;

/**
 * This class provides a set of general purpose utility methods related to weather.
 */

public final class WeatherUtils {

    /**
     * Get wind direction according to the wind degrees.
     *
     * @param degree The wind degree.
     * @return The wind direction.
     */
    public static String getWindDirection(final double degree) {
        String direction = "Unknown";

        if (degree < 22.5 && degree >= 337.5) {
            direction = "N";
        } else if (degree < 67.5 && degree >= 22.5) {
            direction = "NE";
        } else if (degree < 112.5 && degree >= 67.5) {
            direction = "E";
        } else if (degree < 157.5 && degree >= 112.5) {
            direction = "SE";
        } else if (degree < 202.5 && degree >= 157.5) {
            direction = "S";
        } else if (degree < 247.5 && degree >= 202.5) {
            direction = "SW";
        } else if (degree < 292.5 && degree >= 247.5) {
            direction = "W";
        } else if (degree < 337.5 && degree >= 292.5) {
            direction = "NW";
        }

        return direction;
    }

}
