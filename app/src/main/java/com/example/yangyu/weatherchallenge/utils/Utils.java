package com.example.yangyu.weatherchallenge.utils;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

/**
 * This class provides a set of general purpose utility methods.
 */

public final class Utils {

    /**
     * Requests to hide the soft input window from the context of the window that is currently
     * accepting input.
     *
     * @param context The context.
     */
    public static void hideSoftKeyboard(final Activity context) {
        final InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (context.getCurrentFocus() != null) {
            imm.hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(), 0);
        }
    }

}
