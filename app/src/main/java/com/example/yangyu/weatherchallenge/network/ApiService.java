package com.example.yangyu.weatherchallenge.network;

import com.example.yangyu.weatherchallenge.models.Weather;
import com.example.yangyu.weatherchallenge.models.WeatherForecast;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * This class defines the api endpoints used in the app.
 */

public interface ApiService {

    /**
     * Call current weather data for one location.
     *
     * @param city  The city name.
     * @param appId The app id.
     * @return The current weather data for the city.
     */
    @GET("weather")
    Observable<Weather> getCurrentWeather(@Query("q") String city, @Query("APPID") String appId);


    /**
     * Call 5 day / 3 hour forecast data for one location.
     *
     * @param city  The city name.
     * @param appId The app id.
     * @param units The chosen units.
     * @return The weather forecast information for the city.
     */
    @GET("forecast")
    Observable<WeatherForecast> getWeatherForecast(@Query("APPID") String appId,
                                                   @Query("q") String city,
                                                   @Query("units") String units);

}
