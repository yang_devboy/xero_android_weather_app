package com.example.yangyu.weatherchallenge.common;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

/**
 * Defines a common interface for all the presenters in the app.
 */

public class BasePresenter<V extends BaseView> extends MvpBasePresenter<V> {
}
