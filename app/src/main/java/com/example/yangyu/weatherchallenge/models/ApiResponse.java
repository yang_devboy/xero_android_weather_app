package com.example.yangyu.weatherchallenge.models;

import com.google.gson.annotations.SerializedName;

/**
 * The abstraction of the remote api response.
 */

public class ApiResponse {
    @SerializedName("cod")
    private String mCode;
    @SerializedName("message")
    private String mMessage;

    public String getCode() {
        return mCode;
    }

    public String getMessage() {
        return mMessage;
    }

    @Override
    public String toString() {
        return "Code: " + mCode + "\nMessage: " + mMessage;
    }
}
