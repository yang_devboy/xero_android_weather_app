package com.example.yangyu.weatherchallenge.ui;

import com.example.yangyu.weatherchallenge.common.BasePresenter;
import com.example.yangyu.weatherchallenge.di.scopes.PerActivity;
import com.example.yangyu.weatherchallenge.models.WeatherForecast;
import com.example.yangyu.weatherchallenge.network.ApiService;
import com.orhanobut.logger.Logger;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * The presenter for the main activity.
 */

@PerActivity
public final class MainPresenter extends BasePresenter<MainView> {
    private final ApiService mApiService;

    private Disposable mDisposable;

    @Inject
    public MainPresenter(ApiService apiService) {
        mApiService = apiService;
    }

    /**
     * Get the 5 day / 3 hour weather forecast for a city.
     *
     * @param appId The app id required by the api.
     * @param city  The city name.
     * @param units The temperature units.
     */
    void getWeatherForecast(String appId, String city, String units) {
        if (isViewAttached()) {
            getView().showLoading();

            mApiService.getWeatherForecast(appId, city, units)
                    .throttleFirst(500, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.io())
                    .unsubscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<WeatherForecast>() {
                        @Override
                        public void onSubscribe(@NonNull Disposable d) {
                            mDisposable = d;
                        }

                        @Override
                        public void onNext(@NonNull WeatherForecast weatherForecast) {
                            Logger.d("successfully obtained weather forecast information");
                            if (isViewAttached()) {
                                getView().showContent(weatherForecast);
                                getView().hideLoading();
                            }
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            Logger.e("Failed to obtain weather forecast information");
                            if (isViewAttached()) {
                                getView().hideLoading();
                                getView().showError(e);
                            }
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (null != mDisposable && !mDisposable.isDisposed()) {
            Logger.d("disposable will be disposed after detaching view");
            mDisposable.dispose();
        }
    }
}
