package com.example.yangyu.weatherchallenge.utils;

import android.util.AttributeSet;
import android.widget.EditText;

import com.example.yangyu.weatherchallenge.BuildConfig;
import com.example.yangyu.weatherchallenge.TestApp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.robolectric.RuntimeEnvironment.application;
import static com.google.common.truth.Truth.assertThat;

/**
 * Unit tests for the edit text utility methods.
 */

@RunWith(RobolectricTestRunner.class)
@Config(sdk = 21, constants = BuildConfig.class, application = TestApp.class)
public class EditTextUtilsTest {
    private EditText mEditText;

    @Before
    public void setUp() throws Exception {
        AttributeSet attributeSet = Robolectric.buildAttributeSet()
                .addAttribute(android.R.attr.maxLength, "5")
                .build();

        mEditText = new EditText(application, attributeSet);
    }

    @Test
    public void test_getEditText_trim() throws Exception {
        mEditText.setText(" test ");
        assertThat(EditTextUtils.getEditText(mEditText)).isEqualTo("test");
    }

    @Test
    public void test_getEditText_empty() throws Exception {
        mEditText.setText("");
        assertThat(EditTextUtils.getEditText(mEditText)).isEqualTo("");
    }
}