package com.example.yangyu.weatherchallenge;

import com.squareup.leakcanary.RefWatcher;

/**
 * The test application used for Robolectric tests.
 */

public class TestApp extends App {

    @Override
    protected RefWatcher initLeakCanary() {
        // No leakcanary in unit tests.
        return RefWatcher.DISABLED;
    }

}
