package com.example.yangyu.weatherchallenge.utils;

import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

/**
 * Unit tests for the weather utility methods.
 */

public class WeatherUtilsTest {

    @Test
    public void test_getWindDirection_unknown() throws Exception {
        assertThat(WeatherUtils.getWindDirection(-10)).isEqualTo("Unknown");
    }

    @Test
    public void test_getWindDirection_ne() throws Exception {
        assertThat(WeatherUtils.getWindDirection(30)).isEqualTo("NE");
    }

    @Test
    public void test_getWindDirection_e() throws Exception {
        assertThat(WeatherUtils.getWindDirection(90)).isEqualTo("E");
    }

    @Test
    public void test_getWindDirection_se() throws Exception {
        assertThat(WeatherUtils.getWindDirection(112.5)).isEqualTo("SE");
    }

    @Test
    public void test_getWindDirection_s() throws Exception {
        assertThat(WeatherUtils.getWindDirection(202)).isEqualTo("S");
    }

    @Test
    public void test_getWindDirection_sw() throws Exception {
        assertThat(WeatherUtils.getWindDirection(217)).isEqualTo("SW");
    }

    @Test
    public void test_getWindDirection_w() throws Exception {
        assertThat(WeatherUtils.getWindDirection(290)).isEqualTo("W");
    }

    @Test
    public void test_getWindDirection_nw() throws Exception {
        assertThat(WeatherUtils.getWindDirection(292.5)).isEqualTo("NW");
    }

}