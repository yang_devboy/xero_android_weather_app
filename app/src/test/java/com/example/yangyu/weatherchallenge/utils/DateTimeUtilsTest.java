package com.example.yangyu.weatherchallenge.utils;

import org.junit.Test;

import java.util.Locale;

import static com.google.common.truth.Truth.assertThat;

/**
 * Unit tests for the date and time utility methods.
 */
public class DateTimeUtilsTest {

    @Test
    public void test_formatDate_null() throws Exception {
        assertThat(DateTimeUtils.formatDate(null, Locale.US)).isEqualTo("");
    }

    @Test
    public void test_formatDate_invalid() throws Exception {
        assertThat(DateTimeUtils.formatDate("test", Locale.US)).isEqualTo("");
    }

    @Test
    public void test_formatDate_valid() throws Exception {
        assertThat(DateTimeUtils.formatDate("2017-10-18 21:00:00", Locale.US)).isEqualTo("19/10/2017 10:00:00 AM");
    }

}