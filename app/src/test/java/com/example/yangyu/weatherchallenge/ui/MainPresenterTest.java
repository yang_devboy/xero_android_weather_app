package com.example.yangyu.weatherchallenge.ui;

import com.example.yangyu.weatherchallenge.models.WeatherForecast;
import com.example.yangyu.weatherchallenge.network.ApiService;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Observable;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 * A bunch of unit tests for the main presenter.
 */

@RunWith(MockitoJUnitRunner.class)
public class MainPresenterTest {
    @Rule
    public final RxSchedulersOverrideRule schedulers = new RxSchedulersOverrideRule();

    private MainPresenter mMainPresenter;

    @Mock
    private MainView mMainView;

    @Mock
    private ApiService mApiService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mMainPresenter = new MainPresenter(mApiService);
    }

    @Test
    public void test_getWeatherForecast_success() throws Exception {
        mMainPresenter.attachView(mMainView);
        assertTrue(mMainPresenter.isViewAttached());

        when(mApiService.getWeatherForecast(anyString(), anyString(), anyString()))
                .thenReturn(Observable.just(new WeatherForecast()));
        mMainPresenter.getWeatherForecast("appId", "city", "units");

        verify(mMainView).showLoading();
        verify(mMainView).showContent(any(WeatherForecast.class));
        verify(mMainView).hideLoading();
    }

    @Test
    public void test_getWeatherForecast_fail() throws Exception {
        mMainPresenter.attachView(mMainView);
        assertTrue(mMainPresenter.isViewAttached());

        when(mApiService.getWeatherForecast(anyString(), anyString(), anyString()))
                .thenReturn(Observable.<WeatherForecast>error(new RuntimeException()));
        mMainPresenter.getWeatherForecast("appId", "city", "units");

        verify(mMainView).showLoading();
        verify(mMainView).hideLoading();
        verify(mMainView).showError(any(Throwable.class));
    }

    @Test
    public void test_detachView() throws Exception {
        mMainPresenter.attachView(mMainView);
        assertTrue(mMainPresenter.isViewAttached());

        mMainPresenter.detachView(true);
        assertFalse(mMainPresenter.isViewAttached());
    }
}